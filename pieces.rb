#Piece class
# => attr_reader :color, :pos(will need to get from board)

class Piece
  attr_accessor :pos, :possible_moves
  attr_reader :color, :board_obj, :value

  def initialize(color, pos, board)
    @color = color
    #pos in format of [x,y]
    @pos = pos
    @board_obj = board
    @possible_moves = []
    @value = " "
  end

  def within_range?(position)
    position.all? {|coord| coord.between?(0,7)}
  end

  #invalid_move: helper function for list_all_moves
  #[8,0]
  def invalid_move?(position)
    square_el = self.board_obj.board[position[0]][position[1]]

    if square_el.nil? || (square_el.color != self.color)
      return false
    end

    return true
  end

end

class Sliding < Piece
  PERPENDICULAR = [[1, 0], [0, 1], [-1, 0], [0, -1]]
  DIAGONALS = [[1, 1], [1, -1], [-1, 1], [-1, -1]]

  def list_all_moves(directional_array)
    all_moves = []
    directional_array.each do |dir|
      #ex: next_position = [1,5]
      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]

      until !within_range?(next_position) || invalid_move?(next_position)
        all_moves << next_position
        next_position = [next_position[0] + dir[0], next_position[1] + dir[1]]
      end
    end
    #this is an array with all possible moves for current piece
    all_moves
  end

end

class Rook < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "R "
  end

  def possible_moves
    self.possible_moves = list_all_moves(PERPENDICULAR)
  end

end

class Bishop < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "B "
  end

  def possible_moves
    self.possible_moves = list_all_moves(DIAGONALS)
  end

end

class Queen < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "Q "
  end

  def possible_moves
    self.possible_moves = list_all_moves(PERPENDICULAR+DIAGONALS)
  end

end

class Stepping < Piece

  def list_all_moves(positional_array)
    all_moves = []
    positional_array.each do |dir|
      #ex: next_position = either of [1,0](King) or [2,1](Knight)
      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]
      next unless within_range?(next_position)

      unless invalid_move?(next_position)
        all_moves << next_position
      end
    end
    #this is an array with all possible moves for current piece
    all_moves
  end

end

class Knight < Stepping
  KNIGHT_POSITIONALS = [[1,2],[1,-2],[-1,2],[-1,-2],[2,1],[2,-1],[-2,1],[-2,-1]]

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "Ki"
  end

  def possible_moves
    self.possible_moves = list_all_moves(KNIGHT_POSITIONALS)
  end

end

class King < Stepping
  KING_POSITIONALS = [ [1, 0],[-1, 0],[0, 1],[0, -1],[1, 1],
                    [1, -1], [-1, -1], [-1, 1] ]

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "K "


  end

  def possible_moves
    self.possible_moves = list_all_moves(KING_POSITIONALS)
  end

end

class Pawn < Piece
  attr_accessor :first_move

  def initialize(color, pos, board)
    super(color, pos, board)
    @first_move = true
    @value = "P "
  end

  def first_move?
    self.first_move
  end

  def list_all_moves
    all_moves = []
    all_moves += list_front_three
    all_moves << two_spaces_ahead if self.first_move?
  end

  def two_spaces_ahead
    front_direction = [[-1, 0], [-2, 0]] if self.color == "W"
    front_direction = [[1, 0], [2, 0]] if self.color == "B"
    if self.board_obj.board[self.pos[0] + front_direction[0][0]][self.pos[1] + front_direction[0][1]].nil?
      [self.pos[0] + front_direction[1][0], self.pos[1] + front_direction[1][1]]
    else
      []
    end

  end

  def list_front_three
    positional_array = [[-1,0], [-1,1], [-1,-1]] if self.color == "W"
    positional_array = [[1,0], [1,1], [1,-1]] if self.color == "B"

    all_moves = []
    positional_array.each_with_index do |dir,index|
      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]
      next unless within_range?(next_position)

      if index == 0
        all_moves << next_position if self.board_obj.board[next_position[0]][next_position[1]].nil?
      elsif !self.board_obj.board[next_position[0]][next_position[1]].nil?
        all_moves << next_position unless self.board_obj.board[next_position[0]][next_position[1]].color == self.color
      end


    end
    all_moves
  end

  def possible_moves
    self.possible_moves = list_all_moves
  end

end

