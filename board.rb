require './pieces'
require 'debugger'

class Board
  attr_accessor :board, :w_king, :b_king

  def initialize
    @board = Array.new(8) { Array.new(8, nil)}
    populate_pawns
    populate_back_row
  end

  def populate_pawns
    pawn_hash = { 1 => "B", 6 => "W"}

    pawn_hash.each do |key,value|
      self.board[key].each_with_index do |row_el, col|
        self.board[key][col] = Pawn.new(pawn_hash[key], [key, col], self)
      end
    end

  end

  def populate_back_row
    back_row_hash = { 0 => "B", 7 => "W" }
    back_row_hash.each do |key,value|

      self.board[key].each_with_index do |row_el, col|

        if col == 0 || col == 7
          self.board[key][col] = Rook.new(back_row_hash[key], [key, col], self)
        elsif col == 1 || col == 6
          self.board[key][col] = Knight.new(back_row_hash[key], [key, col], self)
        elsif col == 2 || col == 5
          self.board[key][col] = Bishop.new(back_row_hash[key], [key, col], self)
        elsif col == 3
          self.board[key][col] = Queen.new(back_row_hash[key], [key, col], self)
        else
          self.board[key][col] = King.new(back_row_hash[key], [key, col], self)
          if back_row_hash[key] == "B"
            self.b_king = self.board[key][col]
          else
            self.w_king = self.board[key][col]
          end
        end

      end

    end
  end

  #[6,4]
  def move(start, end_pos)
    current_el = self.board[start[0]][start[1]]

    raise "No piece in start position" if current_el.nil?

    raise "Invalid move" unless current_el.possible_moves.include?(end_pos)

    self.board[end_pos[0]][end_pos[1]] = current_el
    current_el.pos = end_pos
    self.board[start[0]][start[1]] = nil
  end

  def render
    print "   "
    8.times {|n| print "#{n} "}
    print "\n"
    self.board.each_with_index do |row, i|
      print " #{i} "
      row.each do |el|
        if el == nil
          print "__"
        else
          print el.value
        end
      end
      print "\n"
    end
  end

  #color must be a string: "W"
  def checked?(color)
    evaluated_king = self.w_king if color == "W"
    evaluated_king = self.b_king if color == "B"

    self.board.each_with_index do |row, row_i|

      row.each_with_index do |el, col|
        next if (el == nil) || (el.color == color)
        return true if el.possible_moves.include?(evaluated_king.pos)
      end

    end
    return false
  end

end

p = Board.new
p.move([6,4],[5,4])
p.move([1,3],[2,3])
p "#{p.checked?("B")} BLAH"
p.move([7,5],[3,1])
p p.board[3][1].possible_moves
p "#{p.checked?("B")} BLAH"
p p.render
